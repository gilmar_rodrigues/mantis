package br.com.mantis.framework;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

public class SelectInteractions extends DriverContext{
	
	public void selecionarPorTextoVisivel(By elemento, String text)
	{
		Select select = new Select(getDriver().findElement(elemento));
		select.selectByVisibleText(text);
	}

}
