package br.com.mantis.framework;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EsperaInteractions extends DriverContext {

	String dado;

	public void esperar(int tempoEmSegundos) {
		try {
			Thread.sleep(tempoEmSegundos * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void esperarElementoExistir(By elemento, int timeOut) {
		WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
		wait.until(ExpectedConditions.presenceOfElementLocated(elemento));
	}

	public void capturarTexto(By elemento) {
		dado = getDriver().findElement(elemento).getText();
	}

	public void esperarTextoMudar(By elemento, int timeOut) {

		if (timeOut <= 0) {
			System.out.println("tabela n�o foi atualizada");
		} else {
			if (getDriver().findElement(elemento).getText().equals(dado)) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				esperarTextoMudar(elemento, --timeOut);
			}
		}
	}
}
