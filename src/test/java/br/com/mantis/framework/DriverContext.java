package br.com.mantis.framework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverContext {
	
	protected static WebDriver driver;

	protected WebDriver getDriver() {
		
		if(driver == null )
		{
			System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
		}
		return driver;
	}

	protected void setDriver(WebDriver driverEntrada) {
		driver = driverEntrada;
	}
	

}
