package br.com.mantis.framework;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class TecladoInteractions extends DriverContext {

	public void escrever(By elemento, String text) {
		getDriver().findElement(elemento).click();
		getDriver().findElement(elemento).clear();
		getDriver().findElement(elemento).sendKeys(text);
	}

	public void tabNoElemento(By elemento)
	{
		getDriver().findElement(elemento).sendKeys(Keys.TAB);
	}

}
