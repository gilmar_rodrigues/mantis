package br.com.mantis.framework;

import org.openqa.selenium.WebDriver;

public class DriverInteractions extends DriverContext{
	
	public void abrirUl(String url)
	{
		getDriver().get(url);
	}
	
	public void fecharNavegador()
	{
		getDriver().quit();
	}
	
	public WebDriver getWebDriver()
	{
		return getDriver();
	}

}
