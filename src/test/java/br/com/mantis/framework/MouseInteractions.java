package br.com.mantis.framework;

import org.openqa.selenium.By;

public class MouseInteractions extends DriverContext{
	
	public void clicar(By elemento)
	{
		getDriver().findElement(elemento).click();
	}
}
