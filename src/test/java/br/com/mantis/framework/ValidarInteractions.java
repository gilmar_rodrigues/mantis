package br.com.mantis.framework;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.openqa.selenium.By;

public class ValidarInteractions extends DriverContext {

	String conteudo;

	public void validarTextContido(By elemento, String texto) {
		String msgTela = getDriver().findElement(elemento).getAttribute("innerText");
		assertTrue("Mensagem de sucesso divergente, Esperado: " + texto + " -  Encontrado: " + msgTela,
				msgTela.contains(texto));
	}

	public void capturarTextoDoElemento(By elemento) {
		conteudo = getDriver().findElement(elemento).getText();
	}

	public void esperarTextoDoElementoMudar(By elemento, int timeout) {
		try {
			if (timeout > 0) {
				if (getDriver().findElement(elemento).getText().equals(conteudo)) {
					Thread.sleep(1000);
					esperarTextoDoElementoMudar(elemento, --timeout);
				}
			} else {
				fail("Texdo to elemento: " + elemento + " nao foi atualizado!");
			}
		} catch (InterruptedException e) {
		}
	}

}
