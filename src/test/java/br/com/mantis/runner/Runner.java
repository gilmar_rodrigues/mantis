package br.com.mantis.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

import org.junit.AfterClass;
import org.junit.runner.RunWith;

import br.com.mantis.framework.DriverInteractions;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty" }, glue = "", monochrome = true, features = "src/test/resources/features"
)
public class Runner {

	@AfterClass
	public static void afterClass() {
		DriverInteractions driver = new DriverInteractions();
		driver.fecharNavegador();
	}

}
