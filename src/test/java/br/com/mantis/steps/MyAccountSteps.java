package br.com.mantis.steps;

import br.com.mantis.framework.EsperaInteractions;
import br.com.mantis.framework.MouseInteractions;
import br.com.mantis.framework.TecladoInteractions;
import br.com.mantis.page.MyAccountPage;
import io.cucumber.java8.En;

public class MyAccountSteps implements En {
	public MyAccountSteps() {
		MouseInteractions mouse = new MouseInteractions();
		TecladoInteractions teclado = new TecladoInteractions();
		EsperaInteractions espera = new EsperaInteractions();

		When("Preencher o password com {string}", (String senha) -> {
			espera.esperarElementoExistir(MyAccountPage.TXT_PASSWORD, 10);
			teclado.escrever(MyAccountPage.TXT_PASSWORD, senha);
		});

		When("Preencher o confirm password com {string}", (String senha) -> {
			espera.esperarElementoExistir(MyAccountPage.TXT_CONFIRM_PASSWORD, 10);
			teclado.escrever(MyAccountPage.TXT_CONFIRM_PASSWORD, senha);
		});

		When("Clicar no botão Update user", () -> {
			espera.esperarElementoExistir(MyAccountPage.BTN_UPDATE_USAR, 10);
			mouse.clicar(MyAccountPage.BTN_UPDATE_USAR);
		});
	}

}
