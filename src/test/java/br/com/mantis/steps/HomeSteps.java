package br.com.mantis.steps;

import br.com.mantis.framework.EsperaInteractions;
import br.com.mantis.framework.MouseInteractions;
import br.com.mantis.framework.SelectInteractions;
import br.com.mantis.page.HomePage;
import io.cucumber.java8.En;

public class HomeSteps implements En {

	public HomeSteps() {
		MouseInteractions mouse = new MouseInteractions();
		EsperaInteractions espera = new EsperaInteractions();
		SelectInteractions select = new SelectInteractions();

		When("Alterar o projeto para {string}", (String projeto) -> {
			espera.esperarElementoExistir(HomePage.SEL_PROJECT, 10);
			espera.capturarTexto(HomePage.TB_ISSUES);
			select.selecionarPorTextoVisivel(HomePage.SEL_PROJECT, projeto);

		});

		Then("Validar mudança de projeto", () -> {
			espera.esperarTextoMudar(HomePage.TB_ISSUES, 10);
		});

		When("Clicar em My Account", () -> {
			espera.esperarElementoExistir(HomePage.LNK_MY_ACCOUNT, 10);
			mouse.clicar(HomePage.LNK_MY_ACCOUNT);
		});

	}

}
