package br.com.mantis.steps;

import br.com.mantis.framework.DriverInteractions;
import br.com.mantis.framework.EsperaInteractions;
import br.com.mantis.framework.MouseInteractions;
import br.com.mantis.framework.TecladoInteractions;
import br.com.mantis.framework.ValidarInteractions;
import br.com.mantis.page.HomePage;
import br.com.mantis.page.LoginPage;
import br.com.mantis.page.Urls;
import io.cucumber.java8.En;

public class LoginSteps implements En {
	DriverInteractions driver = new DriverInteractions();
	MouseInteractions mouse = new MouseInteractions();
	TecladoInteractions teclado = new TecladoInteractions();
	EsperaInteractions espera = new EsperaInteractions();
	ValidarInteractions validacao = new ValidarInteractions();

	public LoginSteps() {
		Given("Abrir home do mantis", () -> {
			driver.abrirUl(Urls.URL_HOME_MANTIS);
		});
		
		When("Preencher o campo username com {string}",(String user)->{
			espera.esperarElementoExistir(LoginPage.TXT_USER_NAME, 10);
			teclado.escrever(LoginPage.TXT_USER_NAME, user);
		});
		
		When("Preencher o campo senha com {string}",(String senha)->{
			espera.esperarElementoExistir(LoginPage.TXT_PASSEWORD, 10);
			teclado.escrever(LoginPage.TXT_PASSEWORD, senha);
		});
		
		When("Desmarcar o secure session",()->{
			espera.esperarElementoExistir(LoginPage.CHK_SECURE_SESSION, 10);
			if(driver.getWebDriver().findElement(LoginPage.CHK_SECURE_SESSION).isSelected())
			{
				mouse.clicar(LoginPage.CHK_SECURE_SESSION);
			}
		});
		
		When("Clicar no botão login",()->{
			mouse.clicar(LoginPage.BTN_LOGIN);
		});
		
		Then("Validar login realizado com sucesso",()->{
			espera.esperarElementoExistir(HomePage.SEL_PROJECT, 10);
		});
		
		Then("Validar mensagem {string}",(String msg)->{
			espera.esperarElementoExistir(LoginPage.LBL_MSG_ERR, 10);
			validacao.validarTextContido(LoginPage.LBL_MSG_ERR, msg);
		});
	}

}
