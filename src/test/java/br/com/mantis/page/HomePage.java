package br.com.mantis.page;

import org.openqa.selenium.By;

public class HomePage {
	
	public static By SEL_PROJECT = By.name("project_id");
	
	public static By TB_ISSUES = By.xpath("//div/table[1]");
	
	public static By LNK_MY_ACCOUNT = By.xpath("//a[.='My Account']");

}
