package br.com.mantis.page;

import org.openqa.selenium.By;

public class LoginPage {
	public static By TXT_USER_NAME = By.name("username");
	public static By TXT_PASSEWORD = By.name("password");
	
	public static By BTN_LOGIN = By.xpath("//input[@value='Login']");
	
	public static By CHK_SECURE_SESSION = By.name("secure_session");
	
	public static By LBL_MSG_ERR = By.xpath("//font[@color='red']");

}
