package br.com.mantis.page;

import org.openqa.selenium.By;

public class MyAccountPage {
	
	public static By TXT_PASSWORD = By.name("password");
	public static By TXT_CONFIRM_PASSWORD = By.name("password_confirm");
	
	public static By BTN_UPDATE_USAR = By.xpath("//input[@value='Update User']");

}
