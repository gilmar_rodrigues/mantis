#Author: gilmarrodriguesst@gmail.com
@MudancaDeSenha
Feature: Mudança de senha

  Scenario: Alterar a senha para testebase21
    When Clicar em My Account
    And Preencher o password com "testebase21"
    And Preencher o confirm password com "testebase21"
    Then Clicar no botão Update user
    And Preencher o campo username com "gilmar.rodrigues"
    And Preencher o campo senha com "testebase21"
    And Desmarcar o secure session
    And Clicar no botão login
    Then Validar login realizado com sucesso

  Scenario: Alterar a senha para testebase2
    When Clicar em My Account
    And Preencher o password com "testebase2"
    And Preencher o confirm password com "testebase2"
    Then Clicar no botão Update user
    And Preencher o campo username com "gilmar.rodrigues"
    And Preencher o campo senha com "testebase2"
    And Desmarcar o secure session
    And Clicar no botão login
    Then Validar login realizado com sucesso
