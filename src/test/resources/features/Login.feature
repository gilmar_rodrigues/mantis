#Author: gilmarrodriguesst@gmail.com
@Login
Feature: Login
  Validação de login valido e login inválido no sistema mantis

  Background: 
    Given Abrir home do mantis

  Scenario Outline: Login inválido
    When Preencher o campo username com <user>
    And Preencher o campo senha com <senha>
    And Desmarcar o secure session
    And Clicar no botão login
    Then Validar mensagem "Your account may be disabled or blocked or the username/password you entered is incorrect."

    Examples: 
      | user               | senha        |
      | "gilmar"           | "teser"      |
      | "gilmar.rodrigues" | "tesetej"    |
      | "gilmar.rodri"     | "testebase2" |

  @MudancaDeProjeto @MudancaDeSenha
  Scenario: Login valido
    When Preencher o campo username com "gilmar.rodrigues"
    And Preencher o campo senha com "testebase2"
    And Desmarcar o secure session
    And Clicar no botão login
    Then Validar login realizado com sucesso
