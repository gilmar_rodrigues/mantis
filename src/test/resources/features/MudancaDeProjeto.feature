#Author: gilmarrodriguesst@gmail.com
@MudancaDeProjeto
Feature: Mudança de projeto

  Scenario Outline: Validar mudança de projeto
    When Alterar o projeto para <projeto>
    Then Validar mudança de projeto

    Examples: 
      | projeto                      |
      | "Gilmar Rodrigues´s Project" |
      | "All Projects"               |
